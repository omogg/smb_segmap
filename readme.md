# smb_segmap

## Overview

This folder contains various launch files for segmap that can be used for loop closure and localization with Super Mega Bot (SMB).  

## Installation

* **eth_supermegabot:** Follow the instructions in [eth_supermegabot](https://github.com/ethz-asl/eth_supermegabot) on how to set up all required software for SMB. Maplab is not necessary.  
* **segmap:** Set up [segmap](https://github.com/ethz-asl/segmap). Install SegmapPy python package.  

## Provided folders

* **launch/smb:** Launch files for segmap.  
* **misc:** Contains config files for smb_confusor and the icp and an example map that can be used for localization.  


## Use smb_segmap for loop closure

The folder smb (contains various launch files) in this package can be directly used with segmap. It should be copied into the segmapper launch folder. Note, that paths of the bag files might need to be adjusted depending on where they were saved.  
The used settings for the packages `ethasl_icp_mapper` and `smb_confusor` can be found in the misc folder.

An example bag file can be found [here](https://polybox.ethz.ch/index.php/s/NRsFTy3aCbGIV4a). Additional bag files are available upon request.

To launch `segmapper`, follow these steps:  

Start python environment:  
```asm
$ source ~/segmappyenv/bin/activate
```
Launch segmapper:  
```asm
$ roslaunch segmapper smb_loop_closure_01.launch
```
Run smb_confusor in new terminal (state estimation):  
```asm
$ rosrun smb_confusor smb_confusor
```
Start icp in new terminal:  
```asm
$ roslaunch ethzasl_icp_mapper supermegabot_dynamic_mapper_segmap.launch
```
Then unpause the bag file.  

![alt_text](misc/SLAM.gif)  
Example of loop closure with segmap.

## Use smb_segmap for localization

Once again, paths for the bag files (bag files are linked above) and the saved map (clausiusstr.pcd) might need to be adjusted.  

For localization follow the same steps as described above but use the localization launch file instead of the loop closure launch file:  
```asm
$ roslaunch segmapper smb_localization.launch
```

## Issues

* Settings do not work well indoors.  
* Not completely integrated into the SMB pipeline.  

